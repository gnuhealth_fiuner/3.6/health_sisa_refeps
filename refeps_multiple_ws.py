# -*- coding: utf-8 -*-
#This file is part health_sisa_refeps module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
try:
    import lxml.etree as etree
except ImportError:
    import xml.etree.cElementTree as etree
import requests

from trytond.pool import Pool
from trytond.transaction import Transaction


class RefepsMultipleWS(object):
    'Census Web Service'

    url_test = 'https://qa.sisa.msal.gov.ar/sisaqa/services/rest/profesional/buscar'
    url = 'https://sisa.msal.gov.ar/sisa/services/rest/profesional/buscar'

    @classmethod
    def get_xml(cls, apellido, nombre, nrodoc, codigo=None):
        User = Pool().get('res.user')

        user = User(Transaction().user)
        payload = {
            'apellido': apellido,
            'nrodoc': nrodoc,
            'usuario': user.sisa_user,
            'clave': user.sisa_password_hidden
            }
        
        if codigo:
            payload['codigo'] = codigo

        url = cls.url
        if user.sisa_mode == 'testing':
            url = cls.url_test

        try:
            response = requests.get(url, params=payload, timeout=30,
                verify=False)
            return etree.fromstring(response.content)
        except Exception:
            return None
