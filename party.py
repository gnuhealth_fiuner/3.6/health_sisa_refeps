# -*- coding: utf-8 -*-
#This file is part health_sisa_refeps module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, Bool


__all__ = ['Party']


class Party(metaclass=PoolMeta):
    "Party"
    __name__ = 'party.party'
    
    refeps_id = fields.Char('REFEPS ID',readonly=True)
    identified_refeps = fields.Boolean('Identified REFEPS', readonly=True,
        states={'invisible': Not(Bool(Eval('is_healthprof')))},
        help='Shows if healthprof party info was checked in REFEPS system')
    non_identified_refeps = fields.Boolean('Non identified on REFEPS',
                                            readonly=True)
    identified_refeps_icon = \
        fields.Function(
            fields.Char('Identified REFEPS Icon'),
                'get_identified_refeps_icon'
                )
    
    last_refeps_check = fields.Date('Last REFEPS check',
                                        readonly= True)
    
    def get_identified_refeps_icon(self,name):
        if self.identified_refeps:
            return 'refeps_icon'
        elif self.non_identified_refeps:
            return 'no_refeps_icon'

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls._buttons.update({
            'get_refeps_data': {
                'invisible': Not(Bool(Eval('is_healthprof'))),
                },
            })

    @staticmethod
    def default_identified_refeps():
        return False

    @classmethod
    @ModelView.button_action('health_sisa_refeps.wizard_refeps_data')
    def get_refeps_data(cls, parties):
        pass
