# -*- coding: utf-8 -*-
#This file is part health_sisa_refeps module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from datetime import datetime, timedelta, date

from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval, Not, Bool
from trytond.pool import Pool, PoolMeta


__all__ = [
        'HealthProfessionalMatriculaEspecialidad',
        'HealthProfessionalMatricula',
        'HealthProfessional', 
        'Appointment'
        ]
    
class HealthProfessionalMatriculaEspecialidad(ModelSQL, ModelView):
    'Health Professional Matricula Especialidad'
    __name__ = 'gnuhealth.healthprofessional.matricula.especialidad'
    
    matricula = fields.Many2One(
                'gnuhealth.healthprofessional.matricula','Matricula',
                required=True, ondelete='CASCADE')
    especialidad = fields.Char('Especialidad', 
                help='Indica la especialidad certificada para la profesión \
                matriculada.',
                readonly=True)
    nroCertificacion = fields.Char('Nro de certificación',help= 'Número de\
                \nregistro otorgado por la Autoridad Sanitaria de Aplicación.',
                readonly=True)
    fechaCertificacion = fields.Char('Fecha de certificación', 
                help='Indica la fecha de creación de la certificación de la\
                \nespecialidad.',
                readonly=True)
    tipoCertificacion = fields.Char('Tipo de certificación',
                help='Indica la modalidad en que se otorgó la certificación:\
                \n Carrera de especialista universitario, Residencia acreditada,\
                \nCertificación por Sociedad Científica, por Examen ante\
                \n autoridad competente, por otra jurisdicción, por profesor\
                \ntitular o adjunto.',
                readonly=True)
    ministerio = fields.Char('Ministerio',
                help='Si la certificación la certificó un ministerio muestra\
                \nel nombre del mismo.'
                ,readonly=True)
    sociedadCientifica = fields.Char('Sociedad científica', 
                help='En caso que el tipo de certificación sea “Certificación\
                \npor Sociedad Científica” indica la Sociedad científica\
                \ndonde se certificó.',
                readonly=True)
    establecimiento = fields.Char('Establecimiento',
                help='Si la certificación la certificó un colegio muestra el\
                \nnombre del mismo.',
                readonly=True)


class HealthProfessionalMatricula(ModelSQL, ModelView):
    'Health Professional Matriculas'
    __name__ = 'gnuhealth.healthprofessional.matricula'
    
    healthprof = fields.Many2One(
                'gnuhealth.healthprofessional', 'Health Professional',
                required=True, ondelete='CASCADE')
    estado = fields.Char('Estado',
                help='Define si la matrícula está habilitada o no.',
                readonly=True)
    matricula = fields.Char('Matrícula', help='Número de matrícula. La\
                \nmatrícula habilita el ejercicio de la profesión dentro del\
                \n territorio de la jurisdicción que la emite.',
                readonly=True)
    jurisdiccion = fields.Char('Jurisdicción',
                help='Provincia que habilita la matrícula.',
                readonly=True)
    profesion = fields.Char('Profesión', help='Denominación de la profesión\
                \ncorrespondiente a la matrícula.',
                readonly=True)
    fechaMatricula = fields.Char('Fecha de Matricula',
                help='Día, mes y año en que se expidió la matrícula.',
                readonly=True)
    fechaModificacionMatricula = fields.Char('Fecha de Modificación de Matricula',
                help='Fecha en la que se modifico en SISA la matrícula.',
                readonly=True)
    fechaRegistroMatricula = fields.Char('Fecha de registro de matricula',
                help='Fecha en la que se registro en SISA la matricula.',
                readonly=True)
    especialidades = fields.One2Many(
        'gnuhealth.healthprofessional.matricula.especialidad', 'matricula', 
        'Especialidades',
        readonly=True)
    provincia = fields.Char('Provincia', readonly=True)
    observaciones = fields.Char('Observaciones', readonly=True)


class HealthProfessional(metaclass = PoolMeta):
    'Health Professional'
    __name__ = 'gnuhealth.healthprofessional'
    
    identified_refeps_icon = fields.Function(
                fields.Selection([
                    ('refeps_icon','Identified REFEPS'),
                    ('non_refeps_icon', 'Non identified REFEPS'),
                    ],'Identified REFEPS icon'),'get_identified_refeps_icon')                   
                    
    identified_refeps = fields.Function(
            fields.Boolean('Identificado en REFEPS'),
                'get_identified_refeps',
                searcher='search_identified_refeps')
    non_identified_refeps = fields.Function(
            fields.Boolean('No identificado en REFEPS'),
                'get_non_identified_refeps',
                searcher='search_non_identified_refeps')
    last_refeps_check = fields.Function(
            fields.Date('Ultima consulta a REFEPS'),
                'get_last_refeps_check',
                searcher='search_last_refeps_check')
    codigo = fields.Char('Code', readonly=True, 
            help="'Tipificación numérica que identifica de\
                \nforma única a cada profesional de salud que se incorpora en\
                \nla Red Federal de Registros de Profesionales de Salud\
                \n(REFEPS) del SISA. Este código se basa en la Resolución\
                \n604/2005 del Ministerio de Salud de la Nación.',")
    matriculas = fields.One2Many(
                'gnuhealth.healthprofessional.matricula', 'healthprof' ,
                'Matrícula', help='Número de matrícula. La\
                \nmatrícula habilita el ejercicio de la profesión dentro del\
                \n territorio de la jurisdicción que la emite.',
                readonly=True)
    fechaRegistro = fields.Char('Fecha de registro',
                help='Fecha de creación del registro del Profesional.',
                readonly=True)
    fechaModificacion = fields.Char('Fecha de modificación',
                help='Fecha de modificación del registro del Profesional.',
                readonly=True)    

    def get_identified_refeps_icon(self,name):
        if self.name.identified_refeps:
            return 'refeps_icon'
        elif self.name.non_identified_renaper:
            return 'non_refeps_icon'
        
    def get_identified_refeps(self,name):
        if self.name.identified_refeps:
            return True
        return False
    
    @classmethod
    def search_identified_refeps(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('name.identified_refeps', clause[1], value))
        return res
    
    def get_non_identified_refeps(self,name):
        if self.name.non_identified_refeps:
            return True
        return False
    
    @classmethod
    def search_non_identified_refeps(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('name.non_identified_refeps', clause[1], value))
        return res
    
    def get_last_sisa_census_check(self,name):
        if self.name.last_refeps_check:
            return self.name.last_refeps_check
        return None
    
    @classmethod
    def search_last_refeps_check(cls,name,clause):
        res = []
        value = clause[2]
        res.append(('name.last_refeps_check',clause[1],value))
        return res


class Appointment(metaclass = PoolMeta):
    'Patient Appointments'
    __name__ = 'gnuhealth.appointment'
    
    identified_refeps_icon = fields.Function(
                fields.Selection([
                    ('refeps_icon','Identified REFEPS'),
                    ('non_refeps_icon', 'Non identified REFEPS'),
                    ],'Identified REFEPS icon'),'get_identified_refeps_icon')
    
    def get_identified_refeps_icon(self,name):
        if self.healthprof and self.healthprof.name.identified_refeps:
            return 'refeps_icon'
        elif self.healthprof and self.healthprof.name.non_identified_refeps:
            return 'non_refeps_icon'
