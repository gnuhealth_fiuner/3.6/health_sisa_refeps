# -*- coding: utf-8 -*-
#This file is part health_sisa_refeps module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys

from datetime import date

from ..refeps_nominal_ws import RefepsNominalWS
from ..refeps_multiple_ws import RefepsMultipleWS

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not

__all__ = ['HealthProfUpdateRefepsWizard']


class HealthProfUpdateRefepsWizard(Wizard):
    'Health Professional Update Refeps Wizard'
    __name__ = 'gnuhealth.healthprofessional.update_refeps'

    @classmethod
    def __setup__(cls):
        super(HealthProfUpdateRefepsWizard,cls).__setup__()
        cls._error_messages.update({
            'ERROR_AUTENTICACION': 'Error en la autenticación del usuario',
            'NO_TIENE_QUOTA_DISPONIBLE': 'El usuario no tiene cuota de uso asignada',
            'REGISTRO_NO_ENCONTRADO': 'La llamada es correcta pero no hay resultado',
            'ERROR': 'La llamada no es correcta o hay otro problema',
            'MULTIPLE_RESULTADO': 'Se encontraron resultados multiples',
            'healthprof_already_created': 'Profesional de Salud ya creado',
            'mora_than_one_healthprof': 'More than one healthprof',
            })

    start_state = 'start'
    start = StateView('gnuhealth.healthprofessional.create.nominal.found',
        'health_sisa_refeps.view_healthprof_create_nominal_found', [
            Button('Update with REFEPS \nHealth Professional data', 'update_healthprof', 'tryton-connect',
                default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
            
    update_healthprof = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        Party = pool.get('party.party')
        Healthprof = pool.get('gnuhealth.healthprofessional')
        res = {}
        if len(Transaction().context['active_ids']) > 1:
            self.raise_user_error('more_than_one_patient')
        healthprof = Healthprof(Transaction().context['active_id'])
        
        name = healthprof.name.name
        lastname = healthprof.name.lastname
        nrodoc = healthprof.name.ref
        gender = healthprof.name.gender
        
        xml_nominal = RefepsNominalWS.get_xml(nombre=name,apellido=lastname,nrodoc=nrodoc)
        if (xml_nominal == None) or (xml_nominal.findtext('resultado') == 'ERROR'):
            self.raise_user_error('ERROR')
        elif xml_nominal.findtext('resultado') == 'ERROR_AUTENTICACION':
            self.raise_user_error('ERROR_AUTENTICACION')
        elif xml_nominal.findtext('resultado') == 'NO_TIENE_QUOTA_DISPONIBLE':
            self.raise_user_error('NO_TIENE_QUOTA_DISPONIBLE')
        elif xml_nominal.findtext('resultado') == 'REGISTRO_NO_ENCONTRADO':
            self.raise_user_error('REGISTRO_NO_ENCONTRADO')
        elif xml_nominal.findtext('resultado') == 'OK':
            matriculas = []
            matricula = {}
            especialidades = []
            especialidad = {}            
            for data in xml_nominal.findall('.//matriculas/matricula'):
                matricula['matricula'] =  data.findtext('matricula')
                matricula['estado'] = data.findtext('estado')
                matricula['jurisdiccion'] = data.findtext('jurisdiccion')
                matricula['profesion'] = data.findtext('profesion')                
                matricula['fechaMatricula'] = data.findtext('fechaMatricula')
                matricula['fechaModificacionMatricula'] = data.findtext('fechaModificacionMatricula')
                matricula['fechaRegistroMatricula'] = data.findtext('fechaRegistroMatricula')
                matricula['provincia'] = data.findtext('provincia')
                matricula['observaciones'] = data.findtext('observaciones')
                especialidades = []
                especialidad = {}
                if data.findall('.//especialidades/especialidad'):
                    for data_esp in data.findall('.//especialidades/especialidad'):
                        especialidad['especialidad'] = data_esp.findtext('especialidad')
                        especialidad['fechaCertificacion'] = data_esp.findtext('fechaCertificacion')
                        especialidad['nroCertificacion'] = data_esp.findtext('nroCertificacion')
                        especialidad['tipoCertificacion'] = data_esp.findtext('tipoCertificacion')
                        especialidad['ministerio'] = data_esp.findtext('ministerio')
                        especialidad['sociedadCientifica'] = data_esp.findtext('sociedadCientifica')
                        especialidad['establecimiento'] = data_esp.findtext('establecimiento')
                        especialidad['sociedadCientifica'] = data_esp.findtext('sociedadCientifica')
                        especialidad['establecimiento'] = data_esp.findtext('establecimiento')
                        especialidades.append(especialidad.copy())
                        especialidad = {}
                    matricula['especialidades'] = especialidades.copy()
                    especialidades = []
                matriculas.append(matricula.copy())
                matricula = {}
            res = {
            'apellido': xml_nominal.findtext('apellido'),
            'nombre': xml_nominal.findtext('nombre'),
            'tipoDocumento': xml_nominal.findtext('tipoDocumento'),
            'numeroDocumento': xml_nominal.findtext('numeroDocumento'),
            'codigo':xml_nominal.findtext('codigo'),
            'matriculas': matriculas,            
            'fechaRegistro': xml_nominal.findtext('fechaRegistro'),
            'fechaModificacion': xml_nominal.findtext('fechaModificacion'),
            'gender': gender,
            }
            return res
        elif xml_nominal.findtext('resultado') == 'MULTIPLE_RESULTADO':
            xml_multiple = RefepsNominalWS.get_xml(nombre=name,apellido=lastname,nrodoc=nrodoc)
            if xml_multiple.findetext('resultado') == 'OK':
                return 'found_multiple'
      
    def transition_update_healthprof(self):
        pool = Pool()        
        HealthProf = pool.get('gnuhealth.healthprofessional')
        healthprof = HealthProf(Transaction().context['active_id'])            
        Party = pool.get('party.party')
                
        #identifier_data = [
                #{'type': 'ar_dni' if self.found_nominal.tipoDocumento == 'DNI' else None,
                #'code': self.found_nominal.numeroDocumento,},
                #{'type': 'refeps_id' if self.found_nominal.codigo else None,
                #'code': self.found_nominal.codigo}
                #]
        
        party_data = {
            'name': self.start.nombre,
            'lastname': self.start.apellido,
            'ref': self.start.numeroDocumento,
            'refeps_id': self.start.codigo,
            'identified_refeps': True,
            'non_identified_refeps': False,
            #'identifiers': [('create', [identifier_data])],
            'last_refeps_check': date.today(),
            }
        Party.write([healthprof.name],party_data)
        
        
        matriculas = []
        matricula = {}
        especialidades = []
        especialidad = {}
        for data in self.start.matriculas:
            matricula['matricula'] = data.matricula
            matricula['jurisdiccion'] = data.jurisdiccion
            matricula['profesion'] = data.profesion
            matricula['estado'] = data.estado
            matricula['fechaMatricula'] = data.fechaMatricula
            matricula['fechaModificacionMatricula'] = data.fechaModificacionMatricula
            matricula['fechaRegistroMatricula'] = data.fechaRegistroMatricula
            matricula['provincia'] = data.provincia
            matricula['observaciones'] = data.observaciones            
            if hasattr(data, 'especialidades'):
                for data_esp in data.especialidades:
                    especialidad['especialidad'] = data_esp.especialidad
                    especialidad['fechaCertificacion'] = data_esp.fechaCertificacion
                    especialidad['nroCertificacion'] = data_esp.nroCertificacion
                    especialidad['tipoCertificacion'] = data_esp.tipoCertificacion
                    especialidad['ministerio'] = data_esp.ministerio
                    especialidad['sociedadCientifica'] = data_esp.sociedadCientifica
                    especialidad['establecimiento'] = data_esp.establecimiento
                    especialidades.append(especialidad.copy())
                    especialidad = {}
                matricula['especialidades'] = [('create',especialidades.copy())]
                especialidades = []
            matriculas.append(matricula.copy())
            matricula = {}
        
        
        HealthProf.write([healthprof],{
            'matriculas': [('delete',[x.id for x in healthprof.matriculas]),('create',matriculas)],
            'code': matriculas[-1]['matricula'],            
            })

        return 'end'
