# -*- coding: utf-8 -*-
#This file is part health_sisa_refeps module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import unicodedata

from datetime import datetime, date

from ..refeps_nominal_ws import RefepsNominalWS
from ..refeps_multiple_ws import RefepsMultipleWS

#from trytond.modules.health_sisa_census.census_ws import CensusWS
#from trytond.modules.health_sisa_puco.puco_ws import PucoWS

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
    Button
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not


__all__ = [
        'HealthProfCreateStart', 
        'HealthProfCreateMatriculaEspecialidad',
        'HealthProfCreateMatricula',
        'HealthProfCreateNominalFound',
        'HealthProfCreateMultipleFound',
        'HealthProfCreateWizard'
        ]


class HealthProfCreateStart(ModelView):
    'Health Professional Create Start'
    __name__ = 'gnuhealth.healthprofessional.create.start'
    
    name = fields.Char("Name", help="Complete name")
    lastname = fields.Char("Lastname", help="Complete lastname")
    nrodoc = fields.Char("PUID", help="Person Unique ID",
                         required=True)
    gender = fields.Selection([
        (None,''),
        ('m','Male'),
        ('f','Female'),
        ],'Gender', required=True)


class HealthProfCreateMatriculaEspecialidad(ModelView):
    'Health Professional Matricula Especialidad'
    __name__ = 'gnuhealth.healthprofessional.create.matricula.especialidad'
    
    especialidad = fields.Char('Especialidad', 
                help='Indica la especialidad certificada para la profesión \
                matriculada.',
                readonly=True)
    nroCertificacion = fields.Char('Nro de certificación',help= 'Número de\
                \nregistro otorgado por la Autoridad Sanitaria de Aplicación.',
                readonly=True)
    fechaCertificacion = fields.Char('Fecha de certificación', 
                help='Indica la fecha de creación de la certificación de la\
                \nespecialidad.',
                readonly=True)
    tipoCertificacion = fields.Char('Tipo de certificación',
                help='Indica la modalidad en que se otorgó la certificación:\
                \n Carrera de especialista universitario, Residencia acreditada,\
                \nCertificación por Sociedad Científica, por Examen ante\
                \n autoridad competente, por otra jurisdicción, por profesor\
                \ntitular o adjunto.',
                readonly=True)
    ministerio = fields.Char('Ministerio',
                help='Si la certificación la certificó un ministerio muestra\
                \nel nombre del mismo.'
                ,readonly=True)
    sociedadCientifica = fields.Char('Sociedad científica', 
                help='En caso que el tipo de certificación sea “Certificación\
                \npor Sociedad Científica” indica la Sociedad científica\
                \ndonde se certificó.',
                readonly=True)
    establecimiento = fields.Char('Establecimiento',
                help='Si la certificación la certificó un colegio muestra el\
                \nnombre del mismo.',
                readonly=True)


class HealthProfCreateMatricula(ModelView):
    'Health Professional Matriculas'
    __name__ = 'gnuhealth.healthprofessional.create.matricula'
    
    estado = fields.Char('Estado',
                help='Define si la matrícula está habilitada o no.',
                readonly=True)
    matricula = fields.Char('Matrícula', help='Número de matrícula. La\
                \nmatrícula habilita el ejercicio de la profesión dentro del\
                \n territorio de la jurisdicción que la emite.',
                readonly=True)
    jurisdiccion = fields.Char('Jurisdicción',
                help='Provincia que habilita la matrícula.',
                readonly=True)
    profesion = fields.Char('Profesión', help='Denominación de la profesión\
                \ncorrespondiente a la matrícula.',
                readonly=True)
    fechaMatricula = fields.Char('Fecha de Matricula',
                help='Día, mes y año en que se expidió la matrícula.',
                readonly=True)
    fechaModificacionMatricula = fields.Char('Fecha de Modificación de Matricula',
                help='Fecha en la que se modifico en SISA la matrícula.',
                readonly=True)
    fechaRegistroMatricula = fields.Char('Fecha de registro de matricula',
                help='Fecha en la que se registro en SISA la matricula.',
                readonly=True)
    especialidades = fields.One2Many(
        'gnuhealth.healthprofessional.create.matricula.especialidad', None, 
        'Especialidades',
        readonly=True)
    provincia = fields.Char('Provincia', readonly=True)
    observaciones = fields.Char('Observaciones', readonly=True)


class HealthProfCreateNominalFound(ModelView):
    'Health Professional Create Nominal Found'
    __name__ = 'gnuhealth.healthprofessional.create.nominal.found'
    
    apellido = fields.Char('Apellido',help='Apellido del profesional.',
                readonly=True)
    nombre = fields.Char('Nombre', help='Nombre del profesional.',
                readonly=True)
    gender = fields.Selection([
        (None,''),
        ('m','Male'),
        ('f','Female'),
        ],'Gender',readonly=True)
    tipoDocumento = fields.Char('Tipo de documento', 
                help='Tipo del documento del profesional.',
                readonly=True)
    numeroDocumento = fields.Char('Número de documento', 
                help='Número de documento del profesional.',
                readonly=True)
    codigo = fields.Char('Código', help='Tipificación numérica que identifica de\
                \nforma única a cada profesional de salud que se incorpora en\
                \nla Red Federal de Registros de Profesionales de Salud\
                \n(REFEPS) del SISA. Este código se basa en la Resolución\
                \n604/2005 del Ministerio de Salud de la Nación.',
                readonly=True)
    matriculas = fields.One2Many(
        'gnuhealth.healthprofessional.create.matricula', None, 'Matrículas',
        readonly=True)
    fechaRegistro = fields.Char('Fecha de registro',
                help='Fecha de creación del registro del Profesional.',
                readonly=True)
    fechaModificacion = fields.Char('Fecha de modificación',
                help='Fecha de modificación del registro del Profesional.',
                readonly=True)


class HealthProfCreateMultipleFound(ModelView):
    'Health Professional Create Multiple Found'
    __name__ = 'gnuhealth.healthprofessional.create.multiple.found'

    healthprof_list = fields.Many2Many(
                'gnuhealth.healthprofessional.create.nominal.found',
                None,None,'Multiples Results')
    
    healthprof_selected = fields.Many2One(
        'gnuhealth.healthprofessional.create.nominal.found',
        'Health Professional Selected',required=True,
        domain=[('id','in',Eval('healthprof_list'))])


class HealthProfCreateWizard(Wizard):
    'Health Professional Create'
    __name__ = 'gnuhealth.healthprofessional.create'
    
    @classmethod
    def __setup__(cls):
        super(HealthProfCreateWizard,cls).__setup__()
        cls._error_messages.update({
            'ERROR_AUTENTICACION': 'Error en la autenticación del usuario',
            'NO_TIENE_QUOTA_DISPONIBLE': 'El usuario no tiene cuota de uso asignada',
            'REGISTRO_NO_ENCONTRADO': 'La llamada es correcta pero no hay resultado',
            'ERROR': 'La llamada no es correcta o hay otro problema',
            'MULTIPLE_RESULTADO': 'Se encontraron resultados multiples',            
            'healthprof_already_created': 'Profesional de Salud ya creado',
            })
    start_state = 'start'
    start = StateView('gnuhealth.healthprofessional.create.start',
        'health_sisa_refeps.view_healthprof_create_start', [
            Button('Check REFEPS', 'check_refeps', 'tryton-connect',
                default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    check_refeps = StateTransition()
    found_nominal = StateView('gnuhealth.healthprofessional.create.nominal.found',
        'health_sisa_refeps.view_healthprof_create_nominal_found', [
            Button('Create Health Professional', 'create_healthprof', 'tryton-ok',
                default=True),
            Button('Search again', 'start', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    found_multiple = StateView('gnuhealth.healthprofessional.create.multiple.found',
        'health_sisa_refeps.view_patient_create_multiple_found', [
            Button('Create Health Professional', 'create_healthprof', 'tryton-ok',
                default=True),
            Button('Search again', 'start', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    create_healthprof = StateAction('health.gnuhealth_action_healthprofessional')
    
    def transition_check_refeps(self):
        pool = Pool()
        HealthProf = pool.get('gnuhealth.healthprofessional')
        ref = self.start.nrodoc.replace('.','').replace(',','')
        healthprof = HealthProf.search([('name.ref','=',self.start.nrodoc)])
        
        if healthprof:
            self.raise_user_error('healthprof_already_created')
        
        name = self.start.name
        lastname = self.start.lastname
        nrodoc = self.start.nrodoc
        gender = self.start.gender
        
        xml_nominal = RefepsNominalWS.get_xml(nombre=name,apellido=lastname,nrodoc=nrodoc)
        if (xml_nominal == None):
            self.raise_user_error('ERROR')
        elif (xml_nominal.findtext('resultado') not in ['OK','MULTIPLE_RESULTADO']):
            self.raise_user_error(xml_nominal.findtext('resultado'))
        elif xml_nominal.findtext('resultado') == 'OK':
            self.found_nominal.apellido = xml_nominal.findtext('apellido')
            self.found_nominal.nombre = xml_nominal.findtext('nombre')
            self.found_nominal.tipoDocumento =\
                xml_nominal.findtext('tipoDocumento')
            self.found_nominal.numeroDocumento =\
                xml_nominal.findtext('numeroDocumento')
            self.found_nominal.codigo =\
                xml_nominal.findtext('codigo')
            self.found_nominal.fechaRegistro= xml_nominal.findtext(\
                'fechaRegistro')
            self.found_nominal.fechaModificacion = xml_nominal.findtext(\
                'fechaModificacion')
            self.found_nominal.gender = gender
            matriculas = []
            matricula = {}
            for data in xml_nominal.findall('.//matriculas/matricula'):
                matricula['matricula'] =  data.findtext('matricula')
                matricula['estado'] = data.findtext('estado')
                matricula['jurisdiccion'] = data.findtext('jurisdiccion')
                matricula['profesion'] = data.findtext('profesion')                
                matricula['fechaMatricula'] = data.findtext('fechaMatricula')
                matricula['fechaModificacionMatricula'] = data.findtext('fechaModificacionMatricula')
                matricula['fechaRegistroMatricula'] = data.findtext('fechaRegistroMatricula')
                matricula['provincia'] = data.findtext('provincia')
                matricula['observaciones'] = data.findtext('observaciones')
                especialidades = []
                especialidad = {}
                if data.findall('.//especialidades/especialidad'):
                    for data_esp in data.findall('.//especialidades/especialidad'):
                        especialidad['especialidad'] = data_esp.findtext('especialidad')
                        especialidad['fechaCertificacion'] = data_esp.findtext('fechaCertificacion')
                        especialidad['nroCertificacion'] = data_esp.findtext('nroCertificacion')
                        especialidad['tipoCertificacion'] = data_esp.findtext('tipoCertificacion')
                        especialidad['ministerio'] = data_esp.findtext('ministerio')
                        especialidad['sociedadCientifica'] = data_esp.findtext('sociedadCientifica')
                        especialidad['establecimiento'] = data_esp.findtext('establecimiento')
                        especialidad['sociedadCientifica'] = data_esp.findtext('sociedadCientifica')
                        especialidad['establecimiento'] = data_esp.findtext('establecimiento')
                        especialidades.append(especialidad.copy())
                        especialidad = {}
                    matricula['especialidades'] = especialidades.copy()
                    especialidades = []
                matriculas.append(matricula.copy())
                matricula = {}
            self.found_nominal.matriculas = matriculas
            return 'found_nominal'
        elif xml_nominal.findtext('resultado') ==  'MULTIPLE_RESULTADO':
            xml_multiple = RefepsNominalWS.get_xml(nombre=name,apellido=lastname,nrodoc=nrodoc)
            if xml_multiple.findetext('resultado') == 'OK':
                return 'found_multiple'
        return 'start'

    def default_found_nominal(self, fields):
        matriculas = []
        matricula = {}
        especialidades = []
        especialidad = {}
        for data in self.found_nominal.matriculas:
            matricula['matricula'] = data.matricula
            matricula['jurisdiccion'] = data.jurisdiccion
            matricula['profesion'] = data.profesion
            matricula['estado'] = data.estado
            matricula['fechaMatricula'] = data.fechaMatricula
            matricula['fechaModificacionMatricula'] = data.fechaModificacionMatricula
            matricula['fechaRegistroMatricula'] = data.fechaRegistroMatricula
            matricula['provincia'] = data.provincia
            matricula['observaciones'] = data.observaciones            
            if hasattr(data, 'especialidades'):
                for data_esp in data.especialidades:
                    especialidad['especialidad'] = data_esp.especialidad
                    especialidad['fechaCertificacion'] = data_esp.fechaCertificacion
                    especialidad['nroCertificacion'] = data_esp.nroCertificacion
                    especialidad['tipoCertificacion'] = data_esp.tipoCertificacion
                    especialidad['ministerio'] = data_esp.ministerio
                    especialidad['sociedadCientifica'] = data_esp.sociedadCientifica
                    especialidad['establecimiento'] = data_esp.establecimiento
                    especialidades.append(especialidad.copy())
                    especialidad = {}
                matricula['especialidades'] = especialidades.copy()
                especialidades = []
            matriculas.append(matricula.copy())
            matricula = {}
        return {
            'apellido': self.found_nominal.apellido,
            'nombre': self.found_nominal.nombre,
            'tipoDocumento': self.found_nominal.tipoDocumento,
            'numeroDocumento': self.found_nominal.numeroDocumento,
            'codigo': self.found_nominal.codigo,
            'matriculas': matriculas,
            'fechaRegistro': self.found_nominal.fechaRegistro,
            'fechaModificacion': self.found_nominal.fechaModificacion,        
            }

    def default_found_multiple(self, fields):
        return {
        
            }
    
    def do_create_healthprof(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        HealthProf = pool.get('gnuhealth.healthprofessional')
        Fedcountry = pool.get('gnuhealth.federation.country.config')(1)

        fed_county = None
        if (Fedcountry and Fedcountry.country):
            fed_country = Fedcountry.code
        
        identifier_data = [
            {'type': 'ar_dni' if self.found_nominal.tipoDocumento == 'DNI' else None,
            'code': self.found_nominal.numeroDocumento,},
            {'type': 'refeps_id' if self.found_nominal.codigo else None,
             'code': self.found_nominal.codigo}
            ]
        
        party = Party.search([('ref','=',self.found_nominal.numeroDocumento)])
        if not party:
            party_data = {
                'name': self.found_nominal.nombre,
                'lastname': self.found_nominal.apellido,
                'ref': self.found_nominal.numeroDocumento,
                'refeps_id': self.found_nominal.codigo,
                'identified_refeps': True,
                'non_identified_refeps': False,
                #'identifiers': [('create', [identifier_data])],
                'gender': self.start.gender,
                'is_person': True,
                'is_healthprof': True,
                'last_refeps_check': date.today(),
                'fed_country': fed_country,
                }
            party = Party.create([party_data])
        else:
            party[0].refeps_id = self.found_nominal.codigo
            party[0].identified_refeps = True
            party[0].non_identified_refeps = False
            party[0].is_healthprof = True
            party[0].last_refeps_check = date.today()
            party[0].save()

        matriculas = []
        matricula = {}
        especialidades = []
        especialidad = {}
        for data in self.found_nominal.matriculas:
            matricula['matricula'] = data.matricula
            matricula['jurisdiccion'] = data.jurisdiccion
            matricula['profesion'] = data.profesion
            matricula['estado'] = data.estado
            matricula['fechaMatricula'] = data.fechaMatricula
            matricula['fechaModificacionMatricula'] = data.fechaModificacionMatricula
            matricula['fechaRegistroMatricula'] = data.fechaRegistroMatricula
            matricula['provincia'] = data.provincia
            matricula['observaciones'] = data.observaciones            
            if hasattr(data, 'especialidades'):
                for data_esp in data.especialidades:
                    especialidad['especialidad'] = data_esp.especialidad
                    especialidad['fechaCertificacion'] = data_esp.fechaCertificacion
                    especialidad['nroCertificacion'] = data_esp.nroCertificacion
                    especialidad['tipoCertificacion'] = data_esp.tipoCertificacion
                    especialidad['ministerio'] = data_esp.ministerio
                    especialidad['sociedadCientifica'] = data_esp.sociedadCientifica
                    especialidad['establecimiento'] = data_esp.establecimiento
                    especialidades.append(especialidad.copy())
                    especialidad = {}
                matricula['especialidades'] = [('create',especialidades.copy())]
                especialidades = []
            matriculas.append(matricula.copy())
            matricula = {}

        healthprof_data = {
            'name': party[0].id,
            'code': self.found_nominal.matriculas[-1].matricula,
            'codigo': self.found_nominal.codigo,
            'matriculas': [('create', matriculas)],
            'fechaRegistro': self.found_nominal.fechaRegistro,
            'fechaModificacion': self.found_nominal.fechaModificacion,
            }

        healthprof = HealthProf.create([healthprof_data])
        
        data = {'res_id': [x.id for x in healthprof]}
        if len(healthprof) == 1:
            action['views'].reverse()
        return action, data
