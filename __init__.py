# -*- coding: utf-8 -*-
#This file is part health_sisa_refeps module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.pool import Pool
from .party import *
from .health import *
from .wizard import *


def register():
    Pool.register(
        Party,
        HealthProfessionalMatriculaEspecialidad,
        HealthProfessionalMatricula,
        HealthProfessional,
        Appointment,
        HealthProfCreateStart,
        HealthProfCreateMatriculaEspecialidad,
        HealthProfCreateMatricula,
        HealthProfCreateNominalFound,
        HealthProfCreateMultipleFound,
        module='health_sisa_refeps', type_='model')
    Pool.register(
        HealthProfCreateWizard,
        HealthProfUpdateRefepsWizard,
        module='health_sisa_refeps', type_='wizard')
